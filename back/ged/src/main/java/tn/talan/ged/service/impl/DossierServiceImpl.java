package tn.talan.ged.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.talan.ged.model.Dossier;
import tn.talan.ged.repository.DossierRepository;
import tn.talan.ged.service.DossierService;



@Service
public class DossierServiceImpl implements DossierService{
	
	@Autowired 
	private DossierRepository dossierRepository;
	
	public Dossier ajouterDossier(Dossier emp) {
		return dossierRepository.save(emp);
	}
	public List<Dossier>ListDossier()
	{
		return dossierRepository.findAll();
	}
	public Dossier updateDossier(Long id, Dossier dossier) {
		if (dossierRepository.findById(id).isPresent()) {
			Dossier e = dossierRepository.findById(id).get();
			e.setNomDossier(dossier.getNomDossier());
			e.setDateCreation(dossier.getDateCreation());
		
			
			dossierRepository.save(e);

			return e;

		} else {
			return null;

		}
	}
	
		
		public void deleteDossier(Long id) {
			Optional<Dossier> e= dossierRepository.findById(id);
			 if(e.isPresent())
			 {
				dossierRepository.deleteById(id); 
			 }
								
			
			
		}
		
		// affichage par id
		public Dossier getDossier(Long id) {

			return dossierRepository.findById(id).get();

		}


}
