package tn.talan.ged.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.talan.ged.model.Document;
@Repository
public interface DocumentRepository extends JpaRepository<Document,Long> {

}
