package tn.talan.ged.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.talan.ged.model.Dossier;
import tn.talan.ged.service.DossierService;

@RestController
@RequestMapping("/dossier")
public class DossierRest {
	@Autowired
	private DossierService dossierService;
    @PostMapping("")
	public Dossier ajouterDossier(@RequestBody Dossier emp) {
		return dossierService.ajouterDossier(emp);
	}
    @GetMapping("")
	public List<Dossier> ListDossier() {
		return dossierService.ListDossier();
	}
    @PutMapping("/{id}")
	public Dossier updateDossier(@PathVariable(value="id") Long id, @RequestBody Dossier dossier) {
		return dossierService.updateDossier(id, dossier);
	}
    @DeleteMapping("/{id}")
	public void deleteDossier(@PathVariable(value="id") Long id) {
		dossierService.deleteDossier(id);
	}
    @GetMapping("/{id}")
	public Dossier getDossier(@PathVariable (value = "id") Long id) {
		return dossierService.getDossier(id);
	}
}
