package tn.talan.ged.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.talan.ged.model.Dossier;
@Repository
public interface DossierRepository extends JpaRepository<Dossier,Long> {

}
