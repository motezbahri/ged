package tn.talan.ged.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.talan.ged.model.Dossier;
import tn.talan.ged.repository.DossierRepository;

public interface DossierService {

	public Dossier ajouterDossier(Dossier emp);

	public List<Dossier> ListDossier();

	public Dossier updateDossier(Long id, Dossier dossier);

	public void deleteDossier(Long id);

	// affichage par id
	public Dossier getDossier(Long id);

}
