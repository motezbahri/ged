package tn.talan.ged.service;

import java.util.List;
import tn.talan.ged.model.Department;


public interface DepartmentService {
	public Department ajouterDepartment(Department emp);

	public List<Department> ListDepartment();

	public Department updateDepartment(Long id, Department department);

	public void deleteDepartment(Long id);

	// affichage par id
	public Department getDepartment(Long id);

}
