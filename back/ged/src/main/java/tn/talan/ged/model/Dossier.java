package tn.talan.ged.model;

import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
public class Dossier  extends Auditable<Dossier> {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String nomDossier;
	private Date dateCreation;

	@ManyToOne
	private Department department;

	@ManyToMany
	private Collection<Categorie> categories;

	@OneToMany(mappedBy = "subFolders", 
			cascade = { CascadeType.ALL }, 
			fetch = FetchType.LAZY)
	private Collection<Dossier> subFolders;

	@ManyToOne(fetch = FetchType.LAZY)
	private Dossier parentFolder;
}
