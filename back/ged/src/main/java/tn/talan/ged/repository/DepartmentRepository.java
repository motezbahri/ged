package tn.talan.ged.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.talan.ged.model.Department;
@Repository
public interface DepartmentRepository extends JpaRepository<Department,Long> {

}
