package tn.talan.ged;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TalanGedApplication {

	public static void main(String[] args) {
		SpringApplication.run(TalanGedApplication.class, args);
	}

}
