package tn.talan.ged.controller;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.talan.ged.model.Employee;
import tn.talan.ged.service.EmployeeService;


@RestController
@RequestMapping("/employee")
public class EmplyeeRest {
	@Autowired
	private EmployeeService employeeService;
    @PostMapping("")
	public Employee ajouterEmployee(@RequestBody Employee emp) {
		return employeeService.ajouterEmployee(emp);
	}
    @GetMapping("")
	public List<Employee> ListEmploye() {
		return employeeService.ListEmploye();
	}
    @PutMapping("/{id}")
	public Employee updateEmployee(@PathVariable(value="id") Long id, @RequestBody Employee employee) {
		return employeeService.updateEmployee(id, employee);
	}
    @DeleteMapping("/{id}")
	public void deleteEmployee(@PathVariable(value="id") Long id) {
		employeeService.deleteEmployee(id);
	}
    @GetMapping("/{id}")
	public Employee getEmployee(@PathVariable (value = "id") Long id) {
		return employeeService.getEmployee(id);
	}
    
    
    
    
	
}
