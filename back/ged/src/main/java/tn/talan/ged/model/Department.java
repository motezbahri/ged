package tn.talan.ged.model;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
public class Department extends Auditable<Department>{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String nomDepartment;

	@OneToMany(mappedBy = "department")
	private Collection<Employee> employees;

	@OneToMany(mappedBy = "department")
	private Collection<Dossier> dossiers;

}
