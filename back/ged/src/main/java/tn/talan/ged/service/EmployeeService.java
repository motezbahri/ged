package tn.talan.ged.service;

import java.security.cert.PKIXRevocationChecker.Option;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import tn.talan.ged.model.Employee;
import tn.talan.ged.repository.EmployeeRepository;

public interface EmployeeService {

	public Employee ajouterEmployee(Employee emp);

	public List<Employee> ListEmploye();

	public Employee updateEmployee(Long id, Employee employee);

	public void deleteEmployee(Long id);

	// affichage par id
	public Employee getEmployee(Long id);

}
