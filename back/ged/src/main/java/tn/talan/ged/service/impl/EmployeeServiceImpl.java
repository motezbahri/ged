package tn.talan.ged.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.talan.ged.model.Employee;
import tn.talan.ged.repository.EmployeeRepository;
import tn.talan.ged.service.EmployeeService;




@Service
public class EmployeeServiceImpl implements EmployeeService{
	@Autowired 
	private EmployeeRepository employeeRepository;
	
	public Employee ajouterEmployee(Employee emp) {
		return employeeRepository.save(emp);
	}
	public List<Employee>ListEmploye()
	{
		return employeeRepository.findAll();
	}
	public Employee updateEmployee(Long id, Employee employee) {
		if (employeeRepository.findById(id).isPresent()) {
			Employee e = employeeRepository.findById(id).get();
			e.setUsername(employee.getUsername());
			e.setEmail(employee.getEmail());
			e.setPassword(employee.getPassword());
			e.setCin(employee.getCin());
			e.setTelephone(employee.getTelephone());
			e.setAdresse(employee.getAdresse());
			e.setRoles(employee.getRoles());
			e.setStatut(employee.getStatut());
			e.setPhoto(employee.getPhoto());

			
			employeeRepository.save(e);

			return e;

		} else {
			return null;

		}
	}
	
		
		public void deleteEmployee(Long id) {
			Optional<Employee> e= employeeRepository.findById(id);
			 if(e.isPresent())
			 {
				employeeRepository.deleteById(id); 
			 }
								
			
			
		}
		
		// affichage par id
		public Employee getEmployee(Long id) {

			return employeeRepository.findById(id).get();

		}

	

}
