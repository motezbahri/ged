package tn.talan.ged.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.talan.ged.model.Department;
import tn.talan.ged.repository.DepartmentRepository;
import tn.talan.ged.service.DepartmentService;


@Service
public class DepartmentServiceImpl implements DepartmentService{

	
	@Autowired 
	private DepartmentRepository departmentRepository;
	
	public Department ajouterDepartment(Department emp) {
		return departmentRepository.save(emp);
	}
	public List<Department>ListDepartment()
	{
		return departmentRepository.findAll();
	}
	public Department updateDepartment(Long id, Department department) {
		if (departmentRepository.findById(id).isPresent()) {
			Department e = departmentRepository.findById(id).get();
			e.setNomDepartment(department.getNomDepartment());
			
			departmentRepository.save(e);

			return e;

		} else {
			return null;

		}
	}
	
		
		public void deleteDepartment(Long id) {
			Optional<Department> e= departmentRepository.findById(id);
			 if(e.isPresent())
			 {
				departmentRepository.deleteById(id); 
			 }
								
			
			
		}
		
		// affichage par id
		public Department getDepartment(Long id) {

			return departmentRepository.findById(id).get();

		}

	
	
	
	
}
