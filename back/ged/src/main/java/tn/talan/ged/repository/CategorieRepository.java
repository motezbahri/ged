package tn.talan.ged.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.talan.ged.model.Categorie;
@Repository
public interface CategorieRepository extends JpaRepository<Categorie,Long> {

}
