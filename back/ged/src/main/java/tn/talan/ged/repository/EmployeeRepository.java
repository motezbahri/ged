package tn.talan.ged.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import tn.talan.ged.model.Employee;
@Repository
public interface EmployeeRepository extends  JpaRepository<Employee,Long> {
	Optional<Employee> findByUsername(String username);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);
}
