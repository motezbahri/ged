package tn.talan.ged.payload.request;

import java.util.Set;

import javax.validation.constraints.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SignupRequest {
    @NotBlank
    private String username;
 
    @NotBlank
    @Email
    private String email;
    
    private Set<String> role;
    
    @NotBlank
    private String password;

}
