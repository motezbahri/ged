package tn.talan.ged.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.talan.ged.model.Department;
import tn.talan.ged.service.DepartmentService;


@RestController
@RequestMapping("/department")
public class DepartmentRest {
	@Autowired
	private DepartmentService departementService;
    @PostMapping("")
	public Department ajouterDepartment(@RequestBody Department emp) {
		return departementService.ajouterDepartment(emp);
	}
    @GetMapping("")
	public List<Department> ListDepartment() {
		return departementService.ListDepartment();
	}
    @PutMapping("/{id}")
	public Department updateDepartment(@PathVariable(value="id") Long id, @RequestBody Department department) {
		return departementService.updateDepartment(id, department);
	}
    @DeleteMapping("/{id}")
	public void deleteDepartment(@PathVariable(value="id") Long id) {
    	departementService.deleteDepartment(id);
	}
    @GetMapping("/{id}")
	public Department getDepartment(@PathVariable (value = "id") Long id) {
		return departementService.getDepartment(id);
	}
}
