export const environment = {
  production: false,
  hmr: true,
  version: '-dev',
  //version: env.npm_package_version + '-dev',
  defaultLanguage: 'en-US',
  supportedLanguages: ['en-US', 'fr-FR'],

  host: 'http://localhost',
  port: 9092,
};